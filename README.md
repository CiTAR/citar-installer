# Description

This repository contains OpenStack-related deployment files for the CiTAR project.


## Preparing Controller-Machine

First, prepare the controller machine by executing:
```
$ ./scripts/prepare.sh
```
This script will build docker-containers for running the deployment in a pre-configured
environment and generate new SSH key-pair to access the installation target machines.


## Setting up Network

Make shure to manually create a network and a security-group, with allowed ingress
connections on ports 22, 80 and 443.


## Configuring Cluster

CiTAR deployment configuration must be defined in `./artifacts/config/citar.yaml`.
For a basic configuration, you can use provided template file:
```
$ cp ./config/citar.yaml.template ./artifacts/config/citar.yaml
```

Next, at least one named cluster configuration must be defined, located at
`./artifacts/config/clusters/<cluster-name>.yaml`. You can use the following
template as a starting point:
```
$ cp ./config/clusters/devel.yaml.template ./artifacts/config/clusters/devel.yaml
```

An example cluster configuration can look like this:
```yaml
os_rev_prefix: 'citar-dev'
os_region: 'Freiburg'
os_network_name: 'citar-network'
os_security_group_name: 'default'
os_admin_user: ubuntu

os_volume_api_url: 'https://api01.fr.bw-cloud.org:8776/v3'

os_auth:
  auth_url: 'https://idm01.bw-cloud.org:5000/v3'
  os_project_domain_name: 'Default'
  os_user_domain_name: 'Default'
  project_name: 'citar@uni-freiburg.de'
  project_id: 'internal-project-id'
  username: 'user@uni-freiburg.de'
  password: 'super-secret-password'

os_min_poolsize: 1
os_max_poolsize: 4

os_disk_image_base: 'Ubuntu 18.04'

os_gateway_flavor_name: 'm1.large'
os_gateway_disk_size_gb: 25

os_emucomp_flavor_name: 'm1.large'
os_emucomp_disk_size_gb: 20
```


## Deploying Cluster

Preconfigured cluster can be deployed by executing:
```
$ ./scripts/cluster.sh up <cluster-name>
```

Where `<cluster-name>` is one of the cluster configurations located
at `./artifacts/config/clusters/<cluster-name>.yaml`


## Updating Cluster

Deployed cluster can be updated by executing:
```
$ ./scripts/cluster.sh up <cluster-name> [<component>...]
```

Where `<cluster-name>` is one of the cluster configurations located at
`./artifacts/config/clusters/<cluster-name>.yaml` and `<component>` is
a space separated list of components to update: `ear`, `docker`, `ui`.


## Destroying Cluster

To teardown/destroy a cluster, execute:
```
$ ./scripts/cluster.sh down <cluster-name>
```

**NOTE:** This will destroy all previously created resources for
preconfigured cluster!
