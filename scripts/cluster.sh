#!/usr/bin/env sh

set -e

__components_as_json() {
    # convert a list of space-separated strings
    # to a JSON object containing an array
    local components="$(echo "$@" | sed 's/ /","/g')"
    printf '{"eaas_update_components":["%s"]}' "${components}"
}

__usage() {
    local cmdname="$(basename -- "$0")"

    echo "
${cmdname} - Script for running cluster deployments

USAGE:
    ${cmdname} <state> <cluster-name> [<component>...]

DESCRIPTION:
    This will start a minimal docker-container
    and run a deployment playbook using Ansible.

ARGUMENTS:
    <state>
        The target state of the cluster-deployment:
        up    - create cluster
        down  - destroy cluster

    <cluster-name>
        Name of the cluster to create/destroy.

    <component>
        Optional, components to update.
"
}


if [ "$1" = '-h' ] || [ "$1" = '--help' ] ; then
    __usage
    exit 0
fi

cd "$(dirname -- "$0")/.."

case "${1:?cluster-state missing!}" in
    up|down)
        state="$1" ;;
    *)
        echo "[ERROR] unsupported cluster-state: $1"
        exit 1 ;;
esac

cluster="${2:?cluster-name missing!}"
shift 2

components="$@"

set - '--verbose' '--inventory' './config/hosts' 'deployment.yaml'

if [ -n "${components}" ] ; then
    set - '--extra-vars' "$(__components_as_json "${components}")" "$@"
fi

exec ./scripts/ansible-runner.sh \
    ansible-playbook \
    --extra-vars "deployment_state=${state}" \
    --extra-vars "cluster_name=${cluster}" \
    "$@"
