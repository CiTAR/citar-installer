.. config-value:: docker

   Configuration options for the EaaS container.

   .. config-value:: image

      :type: string

      Docker image to pull during installation.

   .. config-value:: port

      :type: int

      Public network port for the EaaS service.

   .. config-value:: ssl

      .. config-value:: enabled

         :type: boolean

         Flag for enabling SSL support.


.. config-value:: ui

   Configuration options for the EaaS user-interface.

   .. config-value:: git_branch

      :type: string

      Name of the Git branch to install UI from.

   .. config-value:: http_auth

      HTTP Basic-Auth options.

      .. config-value:: user

         :type: string

         User name to use for login.

      .. config-value:: password

         :type: string

         Password to use for login.


.. config-value:: eaas

   Configuration options for the EaaS service.

   .. config-value:: git_branch

      :type: string

      Name of the Git branch to install EaaS from.
