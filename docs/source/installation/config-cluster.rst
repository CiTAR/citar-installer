.. config-value:: os_rev_prefix

   :type: string

   Name prefix to use for OpenStack resources created during deployment.

.. config-value:: os_region

   :type: string

   OpenStack region to create resources in.

.. config-value:: os_network_name

   :type: string

   OpenStack network to use for connecting cluster's VMs.

.. config-value:: os_security_group_name

   :type: string

   OpenStack security-group allocated for the cluster. Ingress connections on ports
   22, 80 and 443 must be allowed for CiTAR to work properly.

.. config-value:: os_admin_user

   :type: string

   Name of a user with ``sudo`` capabilities, which should be used to setup and
   configure each machine's operating system.

.. config-value:: os_volume_api_url

   :type: string

   URL for OpenStack's Volume-API.

.. config-value:: os_auth

   Authentication parameters for OpenStack APIs.

   .. config-value:: auth_url

      :type: string

      URL for OpenStack's Auth-API.

   .. config-value:: os_project_name

      :type: string

      Existing project's name.

   .. config-value:: os_project_id

      :type: string

      Existing project's ID.

   .. config-value:: os_project_domain_name

      :type: string

      Domain name corresponding to the project.

   .. config-value:: username

      :type: string

      Exisiting user name.

   .. config-value:: password

      :type: string

      User's password.

   .. config-value:: os_user_domain_name

      :type: string

      Domain name corresponding to the user.


.. config-value:: os_min_poolsize

   :type: integer

   Min. number of emucomp VMs to keep running, even if idle.

.. config-value:: os_max_poolsize

   :type: integer

   Max. number of emucomp VMs to run.

.. config-value:: os_disk_image_base

   :type: string

   Base operating-system image to use for all VMs.

.. config-value:: os_gateway_flavor_name

   :type: string

   VM flavor to use for the gateway machine.

.. config-value:: os_gateway_disk_size_gb

   :type: integer

   Disk size in GB to allocate for the gateway machine.

.. config-value:: os_emucomp_flavor_name

   :type: string

   VM flavor to use for all emucomp machines.

.. config-value:: os_emucomp_disk_size_gb

   :type: integer

   Disk size in GB to allocate for each emucomp machine.