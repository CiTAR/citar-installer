.. Setup

====================
Setup and Deployment
====================

CiTAR is a distributed system composed of multiple independent services, with on-demand auto-scaling support.
Since setting up and configuring such a system can be a complex task, we provide an automated installer for
bwCloud and OpenStack based clouds.


OpenStack Deployment
====================

Ansible is used for setting up and configuring a cluster for CiTAR deployment.


Preconditions
-------------

- A supported Linux operating system should be installed on the target machine. Currently Ubuntu 16.04,
  Ubuntu 18.04 and RedHat/CentOS 7 distributions are supported.
- SSH access to this machine with ``sudo`` or root capabilities. Please make sure you do not need
  a password to use sudo.
- At least 10 GB of free disk space for a minimal CiTAR installation. Additional disk space is required
  to run emulators and store disk images.
- The installer requires a ``python`` interpreter to be installed on the target machine. This will be
  handled automatically on supported Linux distributions.

.. note::

   This method requires manual installation of Docker only on the controller-machine, see
   :ref:`Dependencies <docker_install_section>` for more information. Installation of Docker and
   Docker-Compose on target machines will be handled automatically by the installer, if they are
   not pre-installed there.


Preparing Controller-Machine
----------------------------

First, an Ansible controller-machine must be prepared. This controller-machine will coordinate the CiTAR
installation process and should preferably use a Linux operating system, but MacOS should also work.
To prepare the controller-machine execute the following steps:

- Clone the `citar-installer <https://gitlab.com/citar/citar-installer>`_ repository into an empty directory
- Change into this directory and run:

  .. code-block:: sh

    $ ./scripts/prepare.sh

  This script will prepare the repository, build Docker images required for running the installer and generate
  an SSH key-pair for accessing the target machine. During that process you may be asked for ``sudo`` password,
  depending on your Docker installation.

  .. note::

     If you want to use your own exisiting SSH key, then simply copy the public and private key-pair into the
     directory ``./artifacts/ssh`` and rename those keys to ``admin.key`` and ``admin.key.pub``. Please note,
     that symbolic links won't work, because Ansible will be executed in a docker-container, where host's
     symbolic links are not valid!


Setting up Network
------------------

Make shure to manually create a network and a security-group, with allowed ingress connections on ports 22, 80
and 443. You will later need them for the cluster configuration.


Configuring Cluster
-------------------

CiTAR deployment configuration must be defined in ``./artifacts/config/citar.yaml``. For a basic configuration,
you can use provided template file:

.. code-block:: sh

   $ cp ./config/citar.yaml.template ./artifacts/config/citar.yaml

Next, at least one named cluster configuration must be defined and located at
``./artifacts/config/clusters/<cluster-name>.yaml``. You can use the following template as a starting point:

.. code-block:: sh

   $ cp ./config/clusters/devel.yaml.template ./artifacts/config/clusters/devel.yaml

An example cluster configuration can look like this:

.. literalinclude:: examples/cluster.yaml
   :language: yaml

For documentation of all available options, see :ref:`configuration reference <config-reference>`.


Deploying Cluster
-----------------

When everything is configured, the deployment process can be started by running:

.. code-block:: sh

   $ ./scripts/cluster.sh up <cluster-name>

Where ``<cluster-name>`` is one of the cluster configurations located at ``./artifacts/config/clusters/<cluster-name>.yaml``.


When this process is finished, you should be able to access the CiTAR-UI with a browser under the URL ``https://<dns-domain>``.
The ``<dns-domain>`` should match the address you specified in the ``<cluster-name>.yaml`` file. When asked for
login credentials, the ``ui.http_auth.user`` and ``ui.http_auth.password`` you specified in the ``citar.yaml``
file should be used.


Updating Cluster
----------------

Deployed cluster can be updated by executing:

.. code-block:: sh

   $ ./scripts/cluster.sh up <cluster-name> [<component>...]

Where ``<cluster-name>`` is one of the cluster configurations located at ``./artifacts/config/clusters/<cluster-name>.yaml``
and ``<component>`` is a space-separated list of components to update: ``ear``, ``docker``, ``ui``.


Destroying Cluster
------------------

Deployed cluster can be destroyed by executing:

.. code-block:: sh

   $ ./scripts/cluster.sh down <cluster-name>

.. warning:: This will destroy all previously created resources for preconfigured cluster!


.. _config-reference:

Configuration Reference
=======================

File: *citar.yaml*
------------------

All CiTAR-related configuration options are grouped under ``citar`` section, containing 3 subsections:
``docker``, ``ui`` and ``eaas``.

.. include:: config-citar.rst


File: *<cluster-name>.yaml*
---------------------------

OpenStack-related cluster configuration options.

.. include:: config-cluster.rst
