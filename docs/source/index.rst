CiTAR Technical Documentation
=============================

Welcome to CiTAR's technical documentation!

.. toctree::
   :maxdepth: 1

   installation/deployment
